﻿using System;
using System.Collections.Generic;

namespace TestFoundation
{
    [Serializable]
    // Узел дерева разделов теста.
    public class NodeOfSectionTree
    {
        public string ParentName { get; set; }
        public string Name { get; set; }
        public List<QuestionWithAnswers> questionsAndAnswersOfSection;
        public List<NodeOfSectionTree> Children;

        // Конструктор с параметрами.
        public NodeOfSectionTree(string Parent, string Name)
        {
            this.ParentName = Parent;
            this.Name = Name;
            questionsAndAnswersOfSection = new List<QuestionWithAnswers>();
            this.Children = new List<NodeOfSectionTree>();
        }

        /// Перегрузка конструктора, для создания корневого узла.
        /// Специальная команда "root".
        public NodeOfSectionTree(string specialCommandForMakeRoot)
        {
            if (specialCommandForMakeRoot == "root")    // Активация нового дерева.
            {
                ParentName = null;  // Настоящий корень!
                Name = "root";
                questionsAndAnswersOfSection = new List<QuestionWithAnswers>();
                Children = new List<NodeOfSectionTree>();
            }
        }
    }
}
