﻿using System.Collections.Generic;

namespace TestFoundation
{
    // Перечисление для секций меню.
    public enum CurrentMenuSection
    {
        MainMenu,
        PassTest,
        LoadTest,
        EditTest,
        DeleteQuestion,
        EditQuestion,
        CreateNewTest,
        Settings,
        Statistics,
        Exit
    }

    // UI.ProcessData - Рабочие данные.
    public partial class UI
    {
        #region Секция полей состояния работы меню

        public static CurrentMenuSection CurrentMenuSection { get; set; }                   // Актуальный раздел меню.
        public static int CountItemsOfCurrentMenu { get; set; }                             // Актуальное количество пунктов меню.
        private static bool WasBeforeInMenuSettings { get; set; }                           // Флаг для меню "НАСТРОЙКИ".
        public static int UserChoise { get; set; }                                          // Актуальный (последний) выбор пользователя.
        public static int CurrentCursorPosition { get; set; }                               // Актуальная позиция курсора меню.
        public static Test CurrentTest { get; set; }                                        // Текущий тест.

        public static List<string> ListOfTestsFiles { get; set; }                           // Список всех файлов с тестами в каталоге приложения.

        public static bool TestLoaded { get; set; }                                         /// Флаг загрузки теста.
                                                                                            /// Активирует в ГЛАВНОМ МЕНЮ пункты "Пройти тест" и "Редактировать тест".
        #endregion
    }
}
