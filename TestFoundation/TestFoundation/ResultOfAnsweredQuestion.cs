﻿using System;

namespace TestFoundation
{
    [Serializable]
    public class ResultOfAnsweredQuestion
    {
        // Перечисление результата ответа.
        public enum ResultOfAnswer
        {
            Right,
            Wrong,
            NoTimelyAnswer
        }

        private string Question { get; set; }
        private string RightAnswer { get; set; }
        private string UserAnswer { get; set; }
        public ResultOfAnswer Result { get; set; }

        public ResultOfAnsweredQuestion(string question, string rightAnswer, string userAnswer)
        {
            this.Question = question;
            this.RightAnswer = rightAnswer;
            this.UserAnswer = userAnswer;
            if (rightAnswer == userAnswer)
            { Result = ResultOfAnswer.Right; }
            else
            { Result = ResultOfAnswer.Wrong; }
        }

        public ResultOfAnsweredQuestion(string question, string rightAnswer, string userAnswer, bool timeIsUp)
            : this(question, rightAnswer, userAnswer)
        {
            if (timeIsUp)
            {
                Result = ResultOfAnswer.NoTimelyAnswer;
            }
        }

        public void Show()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Write("вопрос > ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("{0}", Question);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("ответ пользователя >> ");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("{0}", UserAnswer);
            if (Result == ResultOfAnswer.Right)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("  правильно\n", Result);
            }
            else if (Result == ResultOfAnswer.Wrong)
            {           
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("  НЕправильно\n", Result);
            }
            else if (Result == ResultOfAnswer.NoTimelyAnswer)
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("  время закончилось\n", Result);
            }
        }
    }
}
