﻿using System;
using System.Collections.Generic;

namespace TestFoundation
{
    [Serializable]
    public class TestResult
    {
        public DateTime DateTimeTestPassed { get; set; }
        public string NameOfTest { get; set; }
        public List<ResultOfAnsweredQuestion> ResultOfAllAnsweredQuestions { get; set; }
        public int CountTrueAnswers { get; set; }
        public int CountFalseAnswers { get; set; }
        public int CountQestionsWithoutAnswer { get; set; }
        public bool IsItTimeTest;

        public TestResult(DateTime dateTimeTestPassed, string nameOfTest, List<ResultOfAnsweredQuestion> resultOfAllAnsweredQuestions, bool isItTimeTest)
        {
            this.DateTimeTestPassed = dateTimeTestPassed;
            this.NameOfTest = nameOfTest;
            this.IsItTimeTest = isItTimeTest;

            foreach (ResultOfAnsweredQuestion result in resultOfAllAnsweredQuestions)
            {
                if (result.Result == ResultOfAnsweredQuestion.ResultOfAnswer.Right)
                {
                    CountTrueAnswers++;
                }
                else if(result.Result == ResultOfAnsweredQuestion.ResultOfAnswer.Wrong)
                {
                    CountFalseAnswers++;
                }
                else
                {
                    CountQestionsWithoutAnswer++;
                }
            }
        }

        // Выводим в консоль результат прохождения теста.
        public void Show()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write(DateTimeTestPassed);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("\tТЕСТ: {0}", NameOfTest);
            Console.Write("РЕЗУЛЬТАТ: правильно ");
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.Write(" {0} ", CountTrueAnswers);
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write(", НЕправильно ");
            Console.BackgroundColor = ConsoleColor.Red;
            Console.Write(" {0} ", CountFalseAnswers);
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine();
        }

        // Возвращаем скомпонованную строку результата.
        public override string ToString()
        {
            if (IsItTimeTest)
            {
                return DateTimeTestPassed + ". ТЕСТ: " + NameOfTest + ". РЕЗУЛЬТАТ: ПРАВИЛЬНО - " + CountTrueAnswers + ", НЕПРАВИЛЬНО - " + CountFalseAnswers + ", ЗАКОНЧИЛОСЬ ВРЕМЯ - " + CountQestionsWithoutAnswer;
            }
            else
            {
                return DateTimeTestPassed + ". ТЕСТ: " + NameOfTest + ". РЕЗУЛЬТАТ: ПРАВИЛЬНО - " + CountTrueAnswers + ", НЕПРАВИЛЬНО - " + CountFalseAnswers;
            }
        }
    }
}
