﻿using System;
using System.Collections.Generic;
using System.Timers;

namespace TestFoundation
{
    public static class TESTexplorer
    {
        private static int cursorPosition;
        private static int countOfChoise;
        private static bool updateScreen;

        public static List<ResultOfAnsweredQuestion> resultOfAllAnsweredQuestions;
        private static List<QuestionWithAnswers> questionsForTest = new List<QuestionWithAnswers>();
        private static List<string> allQuestionsInTestSection = new List<string>();

        private static bool wasAnswer;

        private static Timer aTimer;                    // Таймер.
        private static int countOfSecondForTest;        // Количество секунд на весь тест.
        private const int countOfSecondForQestion = 15; // Количество секунд на один вопрос.

        private static void SetTimer()
        {
            countOfSecondForTest = questionsForTest.Count * countOfSecondForQestion;
            aTimer = new System.Timers.Timer(1000); // Создаем посекундный таймер.
            aTimer.Elapsed += OnTimedEvent;         // Подключаем событие.
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        // Обработчик события по истечению интервала времени.
        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            countOfSecondForTest--;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.SetCursorPosition(0, Console.WindowHeight - 1);
            Console.Write("тест на время:");
            if (countOfSecondForTest < 0)
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.SetCursorPosition(16, Console.WindowHeight - 1);
                Console.Write("ВРЕМЯ ЗАКОНЧИЛОСЬ...");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.SetCursorPosition(16, Console.WindowHeight - 1);
                Console.Write("{0}     ", countOfSecondForTest);
            }
        }

        static void FindAllQuestionsInTestSection(NodeOfSectionTree tree)
        {
            foreach (QuestionWithAnswers qwa in tree.questionsAndAnswersOfSection)
            {
                questionsForTest.Add(qwa);
                allQuestionsInTestSection.Add(tree.Name);
            }

            foreach (NodeOfSectionTree child in tree.Children)
            {
                FindAllQuestionsInTestSection(child);
            }
        }

        public static void AskQuestions(NodeOfSectionTree tree)
        {
            questionsForTest = new List<QuestionWithAnswers>();
            allQuestionsInTestSection = new List<string>();
            resultOfAllAnsweredQuestions = new List<ResultOfAnsweredQuestion>();
            FindAllQuestionsInTestSection(tree);

            // Запуск таймера (если в настройках включен тест на время).
            if (Settings.enableTimedTest)
            {
                SetTimer();
            }

            while (questionsForTest.Count > 0)
            {
                if (Settings.enableTimedTest && countOfSecondForTest <= 0)

                    break;
                cursorPosition = 1;
                updateScreen = true;
                do
                {
                    if (updateScreen)
                    {
                        if (Settings.enableTimedTest)
                        {
                            aTimer.Stop();
                        }
                        ShowFullQuestionInQuestionsForTest(0);
                        updateScreen = false;
                        if (Settings.enableTimedTest)
                        {
                            aTimer.Start();
                        }
                    }
                }
                while (!NeedPressedKey());
                if (wasAnswer)
                {
                    CheckAnswer(questionsForTest[0]);

                    if (Settings.resultAfterEveryUserAnswer)
                    {
                        Console.Clear(); ;
                        if (Settings.enableTimedTest)
                        {
                            aTimer.Stop();
                        }
                        resultOfAllAnsweredQuestions[resultOfAllAnsweredQuestions.Count - 1].Show();
                        Console.ReadKey(true);
                        if (Settings.enableTimedTest)
                        {
                            aTimer.Start();
                        }
                    }
                    questionsForTest.Remove(questionsForTest[0]);
                    allQuestionsInTestSection.Remove(allQuestionsInTestSection[0]);
                }
                else
                {
                    questionsForTest.Add(questionsForTest[0]);
                    allQuestionsInTestSection.Add(allQuestionsInTestSection[0]);
                    questionsForTest.Remove(questionsForTest[0]);
                    allQuestionsInTestSection.Remove(allQuestionsInTestSection[0]);
                }
            }

            // Если в тесте на время остались не отвеченные вопросы, а время закончилось.
            if (questionsForTest.Count > 0 && Settings.enableTimedTest && countOfSecondForTest <= 0)
            {
                while(questionsForTest.Count>0)
                {
                    CheckAnswer(questionsForTest[0]);
                    questionsForTest.Remove(questionsForTest[0]);
                    allQuestionsInTestSection.Remove(allQuestionsInTestSection[0]);
                }
            }

            if (Settings.enableTimedTest)
            {
                aTimer.Dispose();
            }
            if (Settings.resultAfterAllUserAnswers)
            {
                Console.Clear();
                ShowResult();
                Console.ReadKey(true);
            }
        }

        private static void ShowFullQuestionInQuestionsForTest(int index)
        {
            Console.Clear();
            ShowNameOfSection(allQuestionsInTestSection[index]);
            ShowQuestion(questionsForTest[index]);
            ShowAnswers(questionsForTest[index], true);
        }

        public static void ShowNameOfSection(string NameOfSection)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("раздел|");
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.WriteLine(NameOfSection);
        }
         
        public static void ShowQuestion(QuestionWithAnswers qwa)
        {            
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("\t" +
                "вопрос");
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.Write(": ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(qwa.question);
        }

        public static void ShowAnswers(QuestionWithAnswers qwa, bool showCursor) {
            int itemNumber = 0;
            foreach (string answer in qwa.answers)
            {                
                itemNumber++;
                
                if (showCursor && itemNumber == cursorPosition)
                {

                    if (Settings.highlightTheCorrectAnswer == true && itemNumber == qwa.indexOfRightAnswer + 1)
                    {
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.BackgroundColor = ConsoleColor.Green;
                        Console.Write("\t\t{0}", itemNumber);
                        Console.BackgroundColor = ConsoleColor.Black;
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write("\t\t{0}", itemNumber);
                    }
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                    Console.Write("\t\t{0}", itemNumber);
                    Console.ForegroundColor = ConsoleColor.Gray;
                }
                Console.WriteLine("\t{0}", answer);
                Console.BackgroundColor = ConsoleColor.Black;
            }
            countOfChoise = itemNumber;
        }

        public static bool NeedPressedKey()
        {
            ConsoleKeyInfo pressedKey = new ConsoleKeyInfo();
            pressedKey = Console.ReadKey(true);

            switch (pressedKey.Key)
            {
                
                case ConsoleKey.Enter:
                    wasAnswer = true;
                    return true;
                    //break;
                
                case ConsoleKey.Escape:
                    wasAnswer = false;
                    if (Settings.skipCurrentQuestionForNow)
                    {
                        return true;
                    }
                    break;
               
                case ConsoleKey.UpArrow:
                    if (cursorPosition > 1)
                    {                        
                        cursorPosition--;
                        updateScreen = true;                        
                    }                    
                    break;
                
                case ConsoleKey.DownArrow:
                    if (cursorPosition < countOfChoise)
                    {
                        cursorPosition++;                        
                        updateScreen = true;
                    }
                    break;
               
            }
            return false;
        }

        public static void CheckAnswer(QuestionWithAnswers qwa)
        {

            if (countOfSecondForTest<0)
            {
                resultOfAllAnsweredQuestions.Add(new ResultOfAnsweredQuestion(qwa.question, qwa.answers[qwa.indexOfRightAnswer], qwa.answers[cursorPosition - 1], true));
            }
            else
            {
                resultOfAllAnsweredQuestions.Add(new ResultOfAnsweredQuestion(qwa.question, qwa.answers[qwa.indexOfRightAnswer], qwa.answers[cursorPosition - 1]));
            }
        }

        public static void ShowResult()
        {
            Console.ForegroundColor = ConsoleColor.Black;
            Console.BackgroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("Результаты ответов на вопросы.\n");
            Console.ResetColor();
            foreach (ResultOfAnsweredQuestion result in resultOfAllAnsweredQuestions)
            {
                result.Show();
            }
        }

        // РАЗРАБОТКА!
        public static void DeleteQuestion()
        {
        }

        public static void SelectAndEditQuestionsWithAnswers(NodeOfSectionTree tree)
        {
            questionsForTest = new List<QuestionWithAnswers>();
            FindAllQuestionsInTestSection(tree);
            for (int i = 0; i < questionsForTest.Count; i++)
            {
                cursorPosition = questionsForTest[i].indexOfRightAnswer+1;
                ShowFullQuestionInQuestionsForTest(i);
                ShowInfoAboutControlKeys("пропустить - клавиша ESC, редактировать - клавиша ENTER.");
                while (!NeedPressedKey())
                {
                }
                if (wasAnswer)
                {
                    ShowFullQuestionInQuestionsForTest(i);
                    ShowInfoAboutControlKeys("ДA - клавиша \"Y\", НЕТ - клавиша \"N\".");
                    questionsForTest[i] = EditQuestionWithAnswers(questionsForTest[i]);
                }
                else
                {
                    continue;
                }
            }
            if (NeedAnswerYesNo("сохранить изменения?", ConsoleColor.DarkYellow, true))
            {
                Test.SaveTest(UI.CurrentTest);
            }
        }

        public static QuestionWithAnswers EditQuestionWithAnswers(QuestionWithAnswers qwa)
        {
            qwa = EditQuestion(qwa);
            qwa = DeleteVariantsOfAnswer(qwa);
            if (qwa.answers.Count > 0)
            {
                qwa = EditVariantsOfAnswer(qwa);
            }
            qwa = AddVariantsOfAnswer(qwa);
            qwa = SetRightAnswer(qwa);

            ShowInfoAboutEditedQuestionWithAndAnswers(qwa);

            Console.WriteLine("\nдля продолжения нажмите любую клавишу...");
            Console.ReadKey(true);
            return qwa;
        }

        public static QuestionWithAnswers EditQuestion(QuestionWithAnswers qwa)
        {
            ShowHeaderOfOperation("РЕДАКТИРОВАНИЕ ВОПРОСА.");
            if (NeedAnswerYesNo("нужно ли редактировать вопрос?", ConsoleColor.Yellow, false))
            {
                Console.Write("введите новый вариант вопроса");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(" >> ");
                Console.ForegroundColor = ConsoleColor.Cyan;
                qwa.question = Console.ReadLine().Trim();
                Console.ResetColor();
            }
            return qwa;
        }

        public static QuestionWithAnswers DeleteVariantsOfAnswer(QuestionWithAnswers qwa)
        {
            ShowHeaderOfOperation("УДАЛЕНИЕ ОТВЕТОВ.");
            if (NeedAnswerYesNo("нужно ли удалять варианты ответов?", ConsoleColor.Yellow, false))
            {
                List<string> listOfAnswersForDeleting = new List<string>();
                for (int i = 0; i < qwa.answers.Count; i++)
                {
                    
                    string currentQuestionsForAnswerYesNo = @"yдалить ответ """ + qwa.answers[i] + @"""?";
                    if (NeedAnswerYesNo(currentQuestionsForAnswerYesNo, ConsoleColor.White, false))
                    {
                        listOfAnswersForDeleting.Add(qwa.answers[i]);
                    }
                }
                for (int i = 0; i < listOfAnswersForDeleting.Count; i++)
                {
                    qwa.answers.Remove(listOfAnswersForDeleting[i]);
                }
            }            
            return qwa;
        }

        public static QuestionWithAnswers EditVariantsOfAnswer(QuestionWithAnswers qwa)
        {
            ShowHeaderOfOperation("РЕДАКТИРОВАНИЕ ОТВЕТОВ.");
            if (NeedAnswerYesNo("нужно ли редактировать ответы?", ConsoleColor.Yellow, false))
            {
                for (int i = 0; i < qwa.answers.Count; i++)
                {
                    Console.ResetColor();
                    Console.Write("ответ \"{0}\". ", qwa.answers[i].ToUpper());
                    if (NeedAnswerYesNo("редактировать этот ответ?", ConsoleColor.Yellow, false))
                    {
                        Console.Write("введите новый вариант ответа.\t\t>> ");
                        qwa.answers[i] = Console.ReadLine().Trim();
                    }
                }
            }
            return qwa;
        }

        public static QuestionWithAnswers EnterVariantOfAnswer(QuestionWithAnswers qwa)
        {
            int indexOfcurrentAnswer = qwa.answers.Count;
            // Вводим очередной вариант ответа.
            Console.Write("введите ответ №{0}", indexOfcurrentAnswer + 1);
             Console.ForegroundColor = ConsoleColor.Red;
            Console.Write(" >> ");
            Console.ForegroundColor = ConsoleColor.Cyan;
            qwa.answers.Add(Console.ReadLine().Trim());
            Console.ResetColor();

            return qwa;
        }

        public static QuestionWithAnswers AddVariantsOfAnswer(QuestionWithAnswers qwa)
        {
            ShowHeaderOfOperation("ДОБАВЛЕНИЕ ОТВЕТОВ.");

            if (qwa.answers.Count < 2)
            {
                Console.WriteLine("необходимо как минимум 2 ответа на вопрос");
            }

            while (qwa.answers.Count < 2)
            {
                // Вводим очередной вариант ответа.
                qwa = EnterVariantOfAnswer(qwa);
            }

            while (NeedAnswerYesNo("добавить еще один вариант ответа?", ConsoleColor.Yellow, false))
            {
                // Вводим очередной вариант ответа.
                qwa = EnterVariantOfAnswer(qwa);
            }

            return qwa;
        }

        public static QuestionWithAnswers SetRightAnswer(QuestionWithAnswers qwa)
        {
            ShowHeaderOfOperation("ОПРЕДЕЛЯЕМ ПРАВИЛЬНЫЙ ОТВЕТ.");
            do
            {
                ShowQuestion(qwa);
                qwa.indexOfRightAnswer = -1;
                for (int i = 0; i < qwa.answers.Count; i++)
                {
                    Console.ResetColor();
                    Console.Write("ответ: \"{0}\". ", qwa.answers[i].ToUpper());
                    if (NeedAnswerYesNo("это правильный ответ?", ConsoleColor.Yellow, false))
                    {
                        qwa.indexOfRightAnswer = i;
                        break;
                    }
                }
                if (qwa.indexOfRightAnswer <0)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("необходимо определить правильный ответ!");
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("для повтора нажмите любую клавишу...");
                    Console.ResetColor();

                    Console.ReadKey(true);
                }
            } while (qwa.indexOfRightAnswer < 0);
            return qwa;
        }

        public static void ShowInfoAboutEditedQuestionWithAndAnswers(QuestionWithAnswers qwa)
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("\nвопрос \"{0}\" отредактирован.", qwa.question.ToUpper());
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Варианты ответов:");
            cursorPosition = qwa.indexOfRightAnswer + 1;
            ShowAnswers(qwa, true);
            Console.ResetColor();
        }
        
        private static void ShowHeaderOfOperation(string header)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(header);
            Console.ResetColor();
        }

        private static void ShowInfoAboutControlKeys(string infoAboutControlKeys)
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.Write("\nyправление: ");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("{0}", infoAboutControlKeys);
            Console.ResetColor();
        }

        public static bool NeedAnswerYesNo(string messageForAnswerYesOrNO, ConsoleColor messageColor, bool needShowInfoAboutControlKeys)
        {
            ConsoleKeyInfo pressedKey = new ConsoleKeyInfo();

            Console.ForegroundColor = messageColor;
            Console.WriteLine("{0}", messageForAnswerYesOrNO);
            if (needShowInfoAboutControlKeys)
            {
                ShowInfoAboutControlKeys("ДA - клавиша \"Y\", НЕТ - клавиша \"N\".");
            }
            Console.ResetColor();

            while ((pressedKey.Key != ConsoleKey.Y) & (pressedKey.Key != ConsoleKey.N))
            {
                pressedKey = Console.ReadKey(true);
            }

            // Возвращаем результат. Y - true, N - false;
            if (pressedKey.Key == ConsoleKey.Y)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
