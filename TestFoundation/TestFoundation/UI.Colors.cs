﻿using System;

namespace TestFoundation
{
    // UI.Соlors - Данные о цветах меню.
    public partial class UI
    {
        #region Секция полей цветовых настроек

        public static ConsoleColor headerColor = ConsoleColor.Yellow;
        public static ConsoleColor itemColor = ConsoleColor.Blue;
        public static ConsoleColor bodyColor = ConsoleColor.White;
        public static ConsoleColor hintColor = ConsoleColor.DarkRed;
        public static ConsoleColor cursorColor = ConsoleColor.Blue;
        #endregion
    }
}
