﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TestFoundation
{
    // UI.Action - Взаимодействие с пользователем.
    public partial class UI
    {
        #region Секция методов опроса и обработки выбора пользователя.

        // Метод опроса пользовательского выбора.
        public static void GetUserChoise()
        {
            bool needShowMenu = true;               // Флаг необходимости вывода меню в консоль.
            int userChoise = 1;                     // Номер пункта выбранного пользователем.

            if (UI.WasBeforeInMenuSettings == true)
            {
                userChoise = UserChoise;
            }

            UI.CurrentCursorPosition = userChoise;  // Текущее положение курсора меню.

            ConsoleKeyInfo pressedKey = new ConsoleKeyInfo();

            while (pressedKey.Key != ConsoleKey.Enter) // Ждем нажатия клавиши Enter.
            {
                // Избегаем лишнего мерцания. Обновляем меню в консоли только при необходимости!
                if (needShowMenu)
                {
                    SelectShowActualMenu();     // Обновляем меню с новым положением курсора.                    
                    ShowInfoAboutControlKeys(); // Вывод подсказки о клавишах управления.
                    needShowMenu = false;       // Если клавиши управления не нажаты, не выводим меню в консоль.
                }

                pressedKey = Console.ReadKey(true);     // Ловим нажатие клавиш.

                // Изменяем позицию курсора меню.
                switch (pressedKey.Key)
                {
                    case ConsoleKey.UpArrow:
                        userChoise--;
                        needShowMenu = true;
                        break;

                    case ConsoleKey.DownArrow:
                        userChoise++;
                        needShowMenu = true;
                        break;

                    case ConsoleKey.Enter:
                        UI.UserChoise = userChoise;
                        break;
                }

                // Контролируем положение (замыкаем меню).
                if (userChoise == 0)
                {
                    userChoise = UI.CountItemsOfCurrentMenu;
                }
                else if (userChoise > UI.CountItemsOfCurrentMenu)
                {
                    userChoise = 1;
                }

                // Обновляем глобальное поле актуальной позиции курсора меню.
                UI.CurrentCursorPosition = userChoise;
            }

            UserChoise = userChoise; // Обновляем глобальное поле последнего выбора пользователя.

            if (CurrentMenuSection == CurrentMenuSection.Settings)
            {
                WasBeforeInMenuSettings = true;
            }
        }

        // Выбор метода обработки пользовательского выбора (на основании UI.menuSection). 
        public static void SelectExecutingForUserChoise()
        {
            switch (UI.CurrentMenuSection)
            {
                case CurrentMenuSection.MainMenu:       // Обрабатываем выбор пункта "ГЛАВНОЕ МЕНЮ" (Вернуться в ГЛАВНОЕ МЕНЮ).
                    ExecutingMainMenu();
                    break;

                case CurrentMenuSection.LoadTest:       // Обрабатываем выбор пункта "ЗАГРУЗИТЬ ТЕСТ".
                    ExecutingLoadTest();
                    break;

                case CurrentMenuSection.PassTest:       // Обрабатываем выбор пункта "ПРОЙТИ ТЕСТ".
                    ExecutingPassTest();
                    break;

                case CurrentMenuSection.EditTest:       // Обрабатываем выбор пункта "РЕДАКТИРОВАТЬ ТЕСТ".
                    ExecutingEditTest();
                    break;

                case CurrentMenuSection.CreateNewTest:  // Обрабатываем выбор пункта "СОЗДАТЬ НОВЫЙ ТЕСТ".
                    ExecutingCreateNewTest();
                    break;

                case CurrentMenuSection.Settings:       // Обрабатываем выбор пункта "НАСТРОЙКИ".
                    ExecutingSettings();
                    break;

                case CurrentMenuSection.Statistics:     // Обрабатываем выбор пункта "СТАТИСТИКА".
                    ExecutingStatistics();
                    break;

                case CurrentMenuSection.Exit:           // Обрабатываем выбор пункта "ВЫХОД".
                    ExecutingExit();
                    break;
            }
        }
        #endregion

        #region Секция методов определения и обработки разделов меню.

        // Метод обработки ГЛАВНОГО МЕНЮ.
        public static void ExecutingMainMenu()
        {
            switch (UI.UserChoise)
            {
                case 1:                        // Обработка выбора "ПРОЙТИ ТЕСТ".
                    ExecutingPassTest();
                    break;
                case 2:                        // Обработка выбора "РЕДАКТИРОВАТЬ ТЕСТ".
                    ShowMenuEditTest();  
                    break;
                case 3:                        // Показать меню "ЗАГРУЗИТЬ ТЕСТ".
                    ShowMenuLoadTest();
                    break;
                case 4:                         // Показать меню "СОЗДАТЬ НОВЫЙ ТЕСТ".
                    ShowMenuCreateNewTest();
                    break;
                case 5:                         // Показать меню "СТАТИСТИКА".
                    ShowMenuStatistics();
                    break;
                case 6:                         // Показать меню "НАСТРОЙКИ".
                    ShowMenuSettings();
                    break;
                case 7:                         // Показать меню "ВЫХОД".
                    ShowMenuExit();
                    break;
            }
        }

        // Метод обработки меню "ПРОЙТИ ТЕСТ".
        private static void ExecutingPassTest()
        {
            if (UI.TestLoaded)
            {
                bool isItTimeTest = false;
                if (Settings.enableTimedTest)
                {
                    isItTimeTest = true;
                }
                TESTexplorer.AskQuestions(CurrentTest.tree);
                Statistics.UpdateStatistics(new TestResult(DateTime.Now, CurrentTest.nameOfTest, TESTexplorer.resultOfAllAnsweredQuestions, isItTimeTest));
            }
        }

        // Метод обработки меню "ЗАГРУЗИТЬ ТЕСТ".
        private static void ExecutingLoadTest()
        {
            if (UI.UserChoise > ListOfTestsFiles.Count)
            {
                CurrentCursorPosition = 1;
                UI.CurrentMenuSection = CurrentMenuSection.MainMenu;
            }
            else
            {
                CurrentTest = Test.LoadTest(ListOfTestsFiles[UserChoise - 1].TrimEnd(".dat".ToCharArray()));
                CurrentCursorPosition = 1;
                TestLoaded = true;
                UI.CurrentMenuSection = CurrentMenuSection.MainMenu;
            }
        }

        // Метод обработки подменюменю "РЕДАКТИРУЕМ ТЕСТ".
        public static void ExecutingEditTest()
        {
            if (UI.TestLoaded)
            {
                switch (UI.UserChoise)
                {
                    case 1:
                        TESTexplorer.SelectAndEditQuestionsWithAnswers(CurrentTest.tree);
                        break;
                    case 2:
                        Test.AddQuestionsWithAnswerOrSections(CurrentTest.tree);
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 6:
                        UI.CurrentMenuSection = CurrentMenuSection.MainMenu;
                        break;
                }
            }
            
        }


        // Метод обработки меню "СОЗДАТЬ НОВЫЙ ТЕСТ".
        public static void ExecutingCreateNewTest()
        {
            switch (UI.UserChoise)
            {
                case 1:
                    Console.Clear();

                    Test newTest = new Test(UI.EnterTestName());    // Вводим название теста. При сохранении имя файла будет одноименным.

                    Test.AddQuestionsWithAnswerOrSections(newTest.tree);

                    Test.SaveTest(newTest);

                    Console.WriteLine("ВНИМАНИЕ!!! СОЗДАННЫЙ ТЕСТ СОХРАНЕН ПОД ИМЕНЕМ \"{0}.dat\"", newTest.nameOfTest);
                    Console.WriteLine("Для продолжения нажмите любую клавишу...");
                    _ = Console.ReadKey(true);
                    UI.CurrentMenuSection = CurrentMenuSection.MainMenu;
                    break;
                case 2:
                    UI.CurrentCursorPosition = 1;
                    UI.CurrentMenuSection = CurrentMenuSection.MainMenu;
                    break;
            }
        }

        // Метод ввода пользовательского названия теста.
        private static string EnterTestName()
        {
            string testName;
            do
            {
                Console.Clear();
                Console.Write("Введите название теста!\t>> ");
                testName = Console.ReadLine().Trim();
            } while (CheckNameForTest(testName));

            return testName;
        }

        // Проверяем имя на доспустимость.
        private static bool CheckNameForTest(string testName)
        {
            bool nameIsNotValid = false;
            string allInvalidCharsInString = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            char[] allInvalidChars = allInvalidCharsInString.ToArray<char>();

            for (int i = 0; i < allInvalidChars.Length; i++)
            {
                if (testName.Contains(allInvalidChars[i]))
                {
                    nameIsNotValid = true;
                    ShowInfoAboutInvalidChars();
                }
            }
            if (testName == "")
            {
                nameIsNotValid = true;
                ShowInfoAboutEmptyTestName();
            }

            return nameIsNotValid;
        }

        // Вывод в консоль информации о недопустимых символах в имени теста.
        public static void ShowInfoAboutInvalidChars()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("ИМЯ ТЕСТА ИСПОЛЬЗУЕТСЯ В КАЧЕСТВЕ ИМЕНИ ФАЙЛА.");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("ВНИМАНИЕ!!! НЕДОПУСТИМ ВВОД СПЕЦИАЛЬНЫХ СИМВОЛОВ.");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(@"Запрещённые символы Windows(в различных версиях):");
            Console.WriteLine(@"\ — разделитель подкаталогов");
            Console.WriteLine(@"/ — разделитель ключей командного интерпретатора");
            Console.WriteLine(@": — отделяет букву диска или имя альтернативного потока данных");
            Console.WriteLine(@"* — заменяющий символ(маска «любое количество любых символов»)");
            Console.WriteLine(@"? — заменяющий символ(маска «один любой символ»)");
            Console.WriteLine(@""" — используется для указания путей, содержащих пробелы");
            Console.WriteLine(@"< — перенаправление ввода");
            Console.WriteLine(@"> — перенаправление вывода");
            Console.WriteLine(@"| — обозначает конвейер");
            Console.WriteLine(@"+ — (в различных версиях) конкатенация");
            Console.ResetColor();

            Console.WriteLine("Для продолжения нажмите любую клавишу...");
            Console.ReadKey(true);
            Console.Clear();
        }

        // Вывод в консоль информации о недопустимости пустого (либо состоящего только из пробелов) имени теста.
        public static void ShowInfoAboutEmptyTestName()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("ИМЯ ТЕСТА ИСПОЛЬЗУЕТСЯ В КАЧЕСТВЕ ИМЕНИ ФАЙЛА.");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("ВНИМАНИЕ!!! НЕДОПУСТИМО ПУСТОЕ (ЛИБО СОСТОЯЩЕЕ ТОЛЬКО ИЗ ПРОБЕЛОВ) ИМЯ ТЕСТА.");

            Console.ResetColor();

            Console.WriteLine("Для продолжения нажмите любую клавишу...");
            Console.ReadKey(true);
            Console.Clear();
        }

        // Метод ввода создания нового вопроса с ответами.
        public static QuestionWithAnswers EnterNewQuestionWithAnswers()
        {
            Console.Write("Введите новый вопрос.\t>> ");

            string question = Console.ReadLine().Trim();      // Вопрос.
            List<string> answers = new List<string>();        // Варианты ответов.
            int indexOfRightAnswer = 0;                       // Индекс правильного ответа.
            int currentAnswer = 0;          // Индекс текущего ответа.
            bool needMoreAnswers = true;    // Флаг необходимости ещё одного варианта ответа.
            bool isRightAnswer = false;     // Флаг наличия правильного ответа.

            Console.WriteLine("ВВОДИМ ОТВЕТЫ!");
            while (needMoreAnswers)
            {
                // Вводим очередной вариант ответа.
                Console.Write("Введите ответ №{0}.\t>> ", currentAnswer + 1);
                answers.Add(Console.ReadLine().Trim());

                // Если еще нет правильного ответа, то узнаем не является ли текущий ответ правильным.
                if (!isRightAnswer)
                {
                    if (NeedAnswerYesNo("Является ли этот ответ правильным?"))
                    {
                        indexOfRightAnswer = currentAnswer;
                        isRightAnswer = true;
                    }
                }

                if (!(needMoreAnswers = NeedAnswerYesNo("Добавить еще один вариант ответа?")))
                {
                    if (isRightAnswer == false)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Необходимо добавить правильный ответ к вопросу!!!");
                        Console.ResetColor();
                        needMoreAnswers = true;
                    }
                    else needMoreAnswers = false;
                }
                currentAnswer++;
            }

            return new QuestionWithAnswers(question, answers, indexOfRightAnswer);
        }

        // Метод ДА-НЕТ опроса.
        public static bool NeedAnswerYesNo(string messageForAnswerYesOrNO)
        {
            ConsoleKeyInfo pressedKey = new ConsoleKeyInfo();
            // Является ли ответ правильным?

            Console.WriteLine("{0}\nДA - клавиша \"Y\", НЕТ - клавиша \"N\"", messageForAnswerYesOrNO);

            // Ждем нажатия клавиши Y или N.
            while ((pressedKey.Key != ConsoleKey.Y) & (pressedKey.Key != ConsoleKey.N))
            {
                // Ловим нажатие клавиш.
                pressedKey = Console.ReadKey(true);
            }

            // Возвращаем результат. Y - true, N - false;
            if (pressedKey.Key == ConsoleKey.Y)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // Метод обработки меню "НАСТРОЙКИ".
        private static void ExecutingSettings()
        {
            CurrentMenuSection = CurrentMenuSection.Settings;
            switch (UI.UserChoise)
            {
                case 1:
                    Settings.highlightTheCorrectAnswer = !Settings.highlightTheCorrectAnswer;
                    break;
                case 2:
                    Settings.resultAfterEveryUserAnswer = !Settings.resultAfterEveryUserAnswer;
                    break;
                case 3:
                    Settings.resultAfterAllUserAnswers = !Settings.resultAfterAllUserAnswers;
                    break;
                case 4:
                    Settings.skipCurrentQuestionForNow = !Settings.skipCurrentQuestionForNow;
                    break;
                case 5:
                    Settings.enableTimedTest = !Settings.enableTimedTest;
                    break;
                case 6:
                    UserChoise = 1;
                    WasBeforeInMenuSettings = false;
                    UI.CurrentMenuSection = CurrentMenuSection.MainMenu;
                    break;
            }
        }

        // Метод обработки меню "СТАТИСТИКА".
        private static void ExecutingStatistics()
        {
            CurrentMenuSection = CurrentMenuSection.MainMenu;
        }

        // Метод обработки меню "ВЫХОД".
        private static void ExecutingExit()
        {
            switch (UI.UserChoise)
            {
                case 1:
                    CurrentCursorPosition = 1;
                    UI.CurrentMenuSection = CurrentMenuSection.MainMenu;
                    break;
                case 2:
                    Environment.Exit(0);
                    break;
            }
        }
        #endregion
    }
}