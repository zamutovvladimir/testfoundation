﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace TestFoundation
{
    [Serializable]
    public static class Statistics
    {
        public static List<TestResult> AllTestResults { get; set; } // Список результатов прохождения теста.

        public static void Show()
        {
            LoadStatistics();

            foreach (TestResult result in AllTestResults)
            {
                result.Show();
            }
        }

        // Обновление статистики (добавление в файл результатов последнего тестирования).
        public static void UpdateStatistics(TestResult result)
        {
            LoadStatistics();
            AllTestResults.Add(result);
            SaveStatistics();
        }

        // Сохранение статистики (сериализация).
        public static void SaveStatistics()
        {
            // Создаем BinaryFormatter.
            BinaryFormatter formatter = new BinaryFormatter();

            // Ссоздаем поток байт FileStream (бинарный файл).
            using (FileStream fs = new FileStream("statistics.stc", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, AllTestResults);    // Сериализация (сохранение объекта в поток байт).
                fs.Close();                             // Закрываем поток.
            }
        }

        // Загрузка статистики (десериализация).
        public static void LoadStatistics()
        {
            // создаем BinaryFormatter 
            BinaryFormatter formatter = new BinaryFormatter();

            
            if (System.IO.File.Exists(Directory.GetCurrentDirectory() + @"\statistics.stc"))
            {                
                // открываем поток байт (бинарный файл) 
                using (FileStream fs = new FileStream("statistics.stc", FileMode.OpenOrCreate))
                {
                    TestFoundation.Statistics.AllTestResults = (List<TestResult>)formatter.Deserialize(fs);     // Десериализация (создание объекта из потока байт).
                    fs.Close();
                }
            }
            else
            {
                TestFoundation.Statistics.AllTestResults = new List<TestResult>();
            }
        }
    }
}
