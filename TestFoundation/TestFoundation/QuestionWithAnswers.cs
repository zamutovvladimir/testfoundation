﻿using System;
using System.Collections.Generic;

namespace TestFoundation
{
    [Serializable]
    // Класс вопроса с вариантами ответов.
    public class QuestionWithAnswers
    {
        public string question;         // Вопрос.
        public List<string> answers;    // Варианты ответов.
        public int indexOfRightAnswer;  // Индекс правильного ответа.

        public QuestionWithAnswers(string question, List<string> answers, int indexOfRightAnswer)
        {
            this.question = question;
            this.answers = answers;
            this.indexOfRightAnswer = indexOfRightAnswer;
        }
    }
}
