﻿namespace TestFoundation
{
    class Program
    {
        static void Main(string[] args)
        {
            UI.CurrentMenuSection = CurrentMenuSection.MainMenu;    // Начинаем с ГЛАВНОГО МЕНЮ.
            while (true)
            {
                UI.GetUserChoise();                                 // Опрос пользовательского выбора.
                UI.SelectExecutingForUserChoise();                  // Обработка выбора.
            }
        }
    }
}