﻿using System;
using System.Linq;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


namespace TestFoundation
{
    [Serializable]
    // Класс теста, содержащий дерево разделов с вопросами и ответами, а также методы для работы с ними.
    public class Test
    {
        public NodeOfSectionTree tree;          // Каталог содержимого теста.
        public string nameOfTest;               // Название теста (Имя файла для сериализации).
        public static int currentQuestionAsked; // При прохождении теста текущий заданный вопрос.

        // Конструктор класса Test с параметрами.
        public Test(string nameOfTest)
        {            
            this.nameOfTest = nameOfTest;
            tree = new NodeOfSectionTree("root"); // Новый тест - новое дерево.
        }

        #region Общие статические методы класса Test.

        // Выводим в консоль содержимое теста.
        public static void ShowSectionsTree(NodeOfSectionTree tree)
        {
            foreach (QuestionWithAnswers qwa in tree.questionsAndAnswersOfSection)
            {
                Console.WriteLine("ВОПРОС: {0}.", qwa.question);
            }
            foreach (NodeOfSectionTree child in tree.Children)
            {

                if (tree.ParentName == null)
                {
                    Console.WriteLine("Name: {0}. ROOT is my parent.", child.Name, child.ParentName);
                }
                else
                {
                    Console.WriteLine("Name: {0}. Parent: {1}", child.Name, child.ParentName);
                }

                ShowSectionsTree(child);
            }
        }

        // Добавляем в тест вопросы с ответами и/или разделы.
        public static void AddQuestionsWithAnswerOrSections(NodeOfSectionTree tree)
        {
            if (tree.questionsAndAnswersOfSection.Count == 0)
            {
                while (UI.NeedAnswerYesNo("Создать подраздел, раздела \"" + tree.Name + "\"?"))
                {
                    Console.Write("Введите название раздела!\t>> ");
                    string nameOfSection = Console.ReadLine().Trim();
                    tree.Children.Add(new NodeOfSectionTree(tree.Name, nameOfSection));
                }
            }

            if (tree.questionsAndAnswersOfSection.Count > 0 || tree.Children.Count == 0)
            {
                while (UI.NeedAnswerYesNo("Добавить в раздел \"" + tree.Name + "\" новый вопрос?"))
                {
                    tree.questionsAndAnswersOfSection.Add(UI.EnterNewQuestionWithAnswers());
                }
            }

            foreach (NodeOfSectionTree child in tree.Children)
            {
                AddQuestionsWithAnswerOrSections(child);
            }
        }        

        // Сохранение теста (сериализация).
        public static void SaveTest(Test testToSave)
        {
            // Создаем BinaryFormatter.
            BinaryFormatter formatter = new BinaryFormatter();

            // Создаем имя файла.
            string fileNameOfTest = testToSave.nameOfTest + ".dat";

            // Ссоздаем поток байт FileStream (бинарный файл).
            using (FileStream fs = new FileStream(fileNameOfTest, FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, testToSave);    // Сериализация (сохранение объекта в поток байт).
                fs.Close();                             // Закрываем поток.
            }
        }

        // Загрузка теста (десериализация).
        public static Test LoadTest(string nameOfTest)
        {
            // создаем BinaryFormatter 
            BinaryFormatter formatter = new BinaryFormatter();

            // Создаем имя файла.
            string fileNameOfTest = nameOfTest + ".dat";

            Test loadingTest;
            // Oткрываем поток байт (бинарный файл).
            using (FileStream fs = new FileStream(fileNameOfTest, FileMode.OpenOrCreate))
            {
                loadingTest = (Test)formatter.Deserialize(fs);     // Десериализация (создание объекта из потока байт).
                fs.Close();
            }
            return loadingTest;
        }

        // Получаем список файлов с тестами.
        public static void GetListOfTestsFiles()
        {
            UI.ListOfTestsFiles = Directory.GetFiles(Environment.CurrentDirectory, "*.dat").ToList<string>();
            UI.ListOfTestsFiles.ForEach(f => Console.WriteLine(Path.GetFileName(f)));            
        }
        #endregion
    }
}
