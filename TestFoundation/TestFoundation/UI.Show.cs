﻿using System;
using System.Collections.Generic;

namespace TestFoundation
{
    // UI.Show - Методы вывода в консоль.
    public partial class UI
    {
        #region Секция методов вывода меню в консоль.

        // Метод вывода подсказки о клавишах управления в консоль.
        public static void ShowInfoAboutControlKeys()
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.Write("yправление: ");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("перемещение - клавиши ВВЕРХ|ВНИЗ. выбор - клавиша ENTER.");
            Console.ResetColor();
            Console.CursorVisible = false;
        }

        /// Общий метод вывода меню в консоль.
        /// Возвращает: количество пунктов меню.
        public static void ShowMenu(string[] menuItems)
        {
            // Чистим консоль.
            Console.Clear();

            // Выводим заголовок меню
            Console.BackgroundColor = UI.headerColor;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine("     {0}     ", menuItems[0]);
            Console.BackgroundColor = ConsoleColor.Black;

            for (int bodyItems = 1; bodyItems < menuItems.Length; bodyItems++)
            {

                if (UI.CurrentMenuSection == CurrentMenuSection.MainMenu && !UI.TestLoaded && (bodyItems == 1 || bodyItems == 2))
                {
                    UI.itemColor = ConsoleColor.DarkGray;
                    UI.bodyColor = ConsoleColor.DarkGray;
                }
                else if (UI.CurrentMenuSection == CurrentMenuSection.MainMenu && bodyItems == 6)
                {
                    UI.itemColor = ConsoleColor.Blue;
                    UI.bodyColor = ConsoleColor.Gray;
                }
                else
                {
                    UI.itemColor = ConsoleColor.Blue;
                    UI.bodyColor = ConsoleColor.White;
                }

                Console.ForegroundColor = UI.itemColor;
                Console.Write("{0}. ", bodyItems);

                if (UI.CurrentMenuSection == CurrentMenuSection.Settings)
                {
                    switch (bodyItems)
                    {
                        case 1:
                            ShowSwitchState(Settings.highlightTheCorrectAnswer);
                            break;
                        case 2:
                            ShowSwitchState(Settings.resultAfterEveryUserAnswer);
                            break;
                        case 3:
                            ShowSwitchState(Settings.resultAfterAllUserAnswers);
                            break;
                        case 4:
                            ShowSwitchState(Settings.skipCurrentQuestionForNow);
                            break;
                        case 5:
                            ShowSwitchState(Settings.enableTimedTest);
                            break;
                    }
                }
                Console.ForegroundColor = UI.bodyColor;
                if (bodyItems == CurrentCursorPosition)
                {
                    Console.BackgroundColor = UI.cursorColor;
                    Console.WriteLine("{0}", menuItems[bodyItems]);
                    Console.BackgroundColor = ConsoleColor.Black;
                }
                else
                {
                    Console.WriteLine("{0}", menuItems[bodyItems]);
                }
            }
            Console.WriteLine();
            if (UI.TestLoaded)
            {
                Console.ForegroundColor = ConsoleColor.DarkMagenta;
                Console.Write("загружен тест: ");
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine(CurrentTest.nameOfTest);
            }
            UI.CountItemsOfCurrentMenu = menuItems.Length - 1;
        }

        // Вывод в консоль состояния переключателя.
        private static void ShowSwitchState(bool switchState)
        {
            if (switchState)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("включено  ");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("выключено ");
            }
        }

        // Вывод в консоль меню "ГЛАВНОЕ МЕНЮ".
        static public void ShowMainMenu()
        {
            CurrentMenuSection = CurrentMenuSection.MainMenu;
            string[] menuItemNames = { "ГЛАВНОЕ МЕНЮ",
                                        "Пройти тест",
                                        "Редактировать тест",
                                        "Загрузить тест",
                                        "Создать новый тест",
                                        "Статистика",
                                        "Настройки",
                                        "Выход" };
            ShowMenu(menuItemNames);
        }

        // Вывод в консоль меню "ЗАГРУЗИТЬ ТЕСТ".
        public static void ShowMenuLoadTest()
        {
            CurrentMenuSection = CurrentMenuSection.LoadTest;
            Test.GetListOfTestsFiles();

            List<string> menuItemNames = new List<string>();
            menuItemNames.Add("ЗАГРУЗИТЬ ТЕСТ");

            menuItemNames.AddRange(ListOfTestsFiles);
            menuItemNames.Add("Вернуться в ГЛАВНОЕ МЕНЮ");      

            ShowMenu(menuItemNames.ToArray());
        }

        // Вывод в консоль меню "РЕДАКТИРОВАТЬ ТЕСТ".
        public static void ShowMenuEditTest()
        {
            CurrentMenuSection = CurrentMenuSection.EditTest;
            string[] menuItemNames = { "РЕДАКТИРОВАТЬ ТЕСТ",
                                       "Редактировать вопросы и ответы",
                                       "Добавить новые разделы и вопросы с ответами",
                                       "Удалить вопрос",
                                       "Удалить раздел",
                                       "Удалить тест",
                                       "Вернуться в ГЛАВНОЕ МЕНЮ" };
            ShowMenu(menuItemNames);
        }

        // Вывод в консоль меню "СОЗДАТЬ НОВЫЙ ТЕСТ".
        private static void ShowMenuCreateNewTest()
        {
            CurrentMenuSection = CurrentMenuSection.CreateNewTest;
            string[] menuItemNames = { "СОЗДАТЬ НОВЫЙ ТЕСТ",
                                        "Приступить к созданию",
                                        "Вернуться в ГЛАВНОЕ МЕНЮ"};
            ShowMenu(menuItemNames);
        }

        /// РАЗРАБОТКА!
        private static void ShowSubMenuDeleteQuestion()
        {
        }


        // Вывод в консоль меню "НАСТРОЙКИ".
        private static void ShowMenuSettings()
        {
            CurrentMenuSection = CurrentMenuSection.Settings;
            
            string[] menuItemNames = { "НАСТРОЙКИ",
                                        "Подсветить номер правильного ответа",
                                        "Показать результат после каждоко ответа на вопрос",
                                        "Показать результаты после ответа на все вопросы теста",
                                        "Возможноть временно пропускать вопрос (клавиша Esc)",
                                        "Тест на время",                                        
                                        "Вернуться в ГЛАВНОЕ МЕНЮ" };
            ShowMenu(menuItemNames);
        }

        // Вывод в консоль меню "СТАТИСТИКА".
        private static void ShowMenuStatistics()
        {
            Statistics.LoadStatistics();
            CurrentMenuSection = CurrentMenuSection.Statistics;            
            string header = "СТАТИСТИКА";

            List<string> menuItemNames = new List<string>();
            menuItemNames.Add(header);
            foreach (TestResult result in Statistics.AllTestResults)
            {
                menuItemNames.Add(result.ToString());
            }

            menuItemNames.Add("Для возврата в ГЛАВНОЕ МЕНЮ нажмите любую клавишу...");

            ShowMenu(menuItemNames.ToArray());
        }

        // Вывод в консоль меню "ВЫХОД".
        private static void ShowMenuExit()
        {
            CurrentMenuSection = CurrentMenuSection.Exit;

            string[] menuItemNames = { "ВЫХОД",
                                        "Вернуться в ГЛАВНОЕ МЕНЮ",
                                        "Выйти из приложения"};
            ShowMenu(menuItemNames);
            
        }

        // Вывод в консоль актуального меню  (на основании UI.menuSection).
        public static void SelectShowActualMenu()
        {
            switch (UI.CurrentMenuSection)
            {
                case CurrentMenuSection.MainMenu:       // Выводим в консоль меню "ГЛАВНОЕ МЕНЮ".
                    UI.ShowMainMenu();
                    break;
                case CurrentMenuSection.LoadTest:       // Выводим в консоль меню "ЗАГРУЗИТЬ ТЕСТ".
                    UI.ShowMenuLoadTest();
                    break;
                case CurrentMenuSection.EditTest:       // Обрабатываем выбор подпункта "РЕДАКТИРОВАТЬ ТЕСТ".
                    UI.ShowMenuEditTest();
                    break;
                case CurrentMenuSection.CreateNewTest:  // Выводим в консоль меню "СОЗДАТЬ НОВЫЙ ТЕСТ".
                    UI.ShowMenuCreateNewTest();
                    break;
                case CurrentMenuSection.Settings:       // Выводим в консоль меню "НАСТРОЙКИ".
                    UI.ShowMenuSettings();
                    break;   
                case CurrentMenuSection.Statistics:     // Выводим в консоль меню "СТАТИСТИКА".
                    UI.ShowMenuStatistics();
                    break;
                case CurrentMenuSection.Exit:           // Выводим в консоль меню "ВЫХОД".
                    UI.ShowMenuExit();
                    break;
            }
        }
        #endregion
    }
}
