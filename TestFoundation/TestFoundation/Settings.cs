﻿namespace TestFoundation
{
    static class Settings
    {
        public static bool highlightTheCorrectAnswer = true;
        public static bool resultAfterEveryUserAnswer = true;
        public static bool resultAfterAllUserAnswers = true;
        public static bool skipCurrentQuestionForNow = true;
        public static bool enableTimedTest = false;
    }
}
